(function($){

  function percentOff(){
    if(parseFloat($('#id_value1').val()) > 100){
      $('#id_value1').val('100');
    };
    $('#id_value1').attr('max', '100');
    $('#id_value2').val('');
    $('.field-value1').show();
    $('.field-value2').hide();
    $('label[for=id_value1]').text('Percentual desconto:')
  }

  function valuePer(){
    $('#id_value1').attr('max', '100000000');
    $('.field-value2').show();
    $('#id_value2').attr('required', 'required');
    $('label[for=id_value1]').text('Valor DE:')
    $('label[for=id_value2]').text('Valor POR:')
  }

  $(document).ready(function(){
    $('#id_promo_type').change(function(){
      var type = $(this).val();
      if('PERCENT_OFF' === type){
        percentOff();
      }else if('VALUE_PER' === type){
        valuePer();
      }else{
        $('.field-value1').hide();
        $('.field-value2').hide();
      }
    });
    $('#id_promo_type').change();
  });
})($);