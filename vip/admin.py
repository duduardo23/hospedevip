from django.contrib import admin
from django import forms
from .models import Store, Promotion, City, PromotionImage
# Register your models here.


class PromotionImageInline(admin.TabularInline):
    model = PromotionImage
    extra = 0

    def get_queryset(self, request):
        qs = super(PromotionImageInline, self).get_queryset(request)
        return qs.order_by('position')


class PromotionAdminInline(admin.StackedInline):
    model = Promotion
    extra = 0
    inlines = [PromotionImageInline, ]


class StoreAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ('name', 'city', 'phone_number', 'site', 'email')
    exclude = ['slug']
    inlines = [PromotionAdminInline, ]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(admin_users__in=[request.user])

    def get_exclude(self, request, obj=None):
        is_superuser = request.user.is_superuser
        if not is_superuser:
            return self.exclude + ['admin_users']
        return self.exclude



class PromotionForm(forms.ModelForm):
    class Meta:
        modeL = Promotion

    def clean(self):
        if not self.cleaned_data.get('promo_type'):
            return
        if not self.cleaned_data.get('value1'):
            return
        if self.cleaned_data['promo_type'] == 'PERCENT_OFF':
            return
        if self.cleaned_data['value2'] is None:
            raise forms.ValidationError({'value2': 'Este campo é obrigatório.'})
        if self.cleaned_data['value2'] >= self.cleaned_data['value1']:
            raise forms.ValidationError({'value2': 'O valor "POR" deve ser menor que o valor "DE".'})

class PromotionAdmin(admin.ModelAdmin):
    search_fields = ['title']
    list_display = ('store', 'title', 'active', 'start_date', 'end_date')
    inlines = [PromotionImageInline]
    form = PromotionForm

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        stores = Store.objects.filter(admin_users__in=[request.user])
        return qs.filter(store__in=stores)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        is_superuser = request.user.is_superuser
        if not is_superuser and db_field.name == "store":
            kwargs["queryset"] =  Store.objects.filter(admin_users__in=[request.user])
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    class Media:
        js = ('vip/js/admin/promotion.js',)

class CityAdmin(admin.ModelAdmin):
    pass


admin.site.register(Store, StoreAdmin)
admin.site.register(Promotion, PromotionAdmin)
admin.site.register(City, CityAdmin)
