from django.test import TestCase
from django.utils import timezone
import datetime
from .models import Promotion


class PromotionModelTests(TestCase):
    def test_isactive_with_future_start_Date(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is in the future.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        promotion = Promotion(enabled=True, start_date=time)
        self.assertIs(promotion.active(), False)
