from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, Http404
from .models import Store, Promotion
from django.db import connection
# Create your views here.


def index(request):
    return render(request, 'vip/index.html', {
        'stores':  Store.objects.all(),
        'promotions':  Promotion.objects.all(),
    })


def store(request, slug):
    store = get_object_or_404(Store, slug=slug)
    return render(request, 'vip/store.html', {
        'store': store
    })


def promotion(request, promotion_id):
    promotion = get_object_or_404(Promotion, pk=promotion_id)

    return render(request, 'vip/promotion.html', {
        'promotion': promotion
    })
