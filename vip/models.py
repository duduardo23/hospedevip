from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from enum import Enum
from django.contrib.auth import get_user_model
from django.utils.formats import localize


class City(models.Model):
    name = models.CharField('nome', max_length=200)
    uf = models.CharField('UF', max_length=2)

    class Meta:
        verbose_name = 'Cidade'
        verbose_name_plural = 'Cidades'

    def __str__(self):
        return self.name


class StoreCategory(Enum):
    RESTAURANT = 'Restaurantes'
    PASSEIOS = 'Passeios'


class Store(models.Model):
    name = models.CharField('nome', max_length=200)
    slug = models.SlugField(unique=True, blank=True)
    description = models.TextField('descrição', null=True, blank=True)
    address1 = models.CharField('endereço', max_length=200)
    address2 = models.CharField('numero', max_length=200)
    address3 = models.CharField(
        'complemento', max_length=200, null=True, blank=True)
    district = models.CharField('bairro', max_length=200)
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    postal_code = models.CharField('CEP', max_length=15)
    phone_number = models.CharField('telefone', max_length=25)
    site = models.URLField('site', null=True, blank=True)
    email = models.EmailField('email', null=True, blank=True)
    category = models.CharField('Categoria', max_length=50,
                                choices=[(t.name, t.value) for t in StoreCategory]
                                )
    admin_users = models.ManyToManyField(get_user_model())

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Store, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Estabelecimento'
        verbose_name_plural = 'Estabelecimentos'


class StoreImage(models.Model):
    promotion = models.ForeignKey(
        Store, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField('imagem', upload_to='images')
    position = models.PositiveSmallIntegerField('posição', default=0)

    class Meta:
        verbose_name = 'Imagem estabelecimento'
        verbose_name_plural = 'Imagens do estabelecimento'

    def __str__(self):
        return str(self.position)


class PromotionType(Enum):
    PERCENT_OFF = 'Percentual de desconto'
    VALUE_PER = 'Valor de/por'


class Promotion(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    description = models.TextField('descrição')
    enabled = models.BooleanField('ativa')
    start_date = models.DateTimeField('data inicio', null=True, blank=True)
    end_date = models.DateTimeField('data de fim', null=True, blank=True)

    promo_type = models.CharField('Tipo', max_length=50,
                                  choices=[(t.name, t.value) for t in PromotionType]
                                  )
    value1 = models.DecimalField('valor', max_digits=7, decimal_places=2)
    value2 = models.DecimalField(
        'valor', max_digits=7, decimal_places=2, null=True, blank=True)

    def active(self):
        if self.start_date is not None:
            current_date = timezone.now()
            return self.enabled and self.start_date > current_date and current_date < self.end_date
        return self.enabled

    active.admin_order_field = 'start_date'
    active.boolean = True
    active.short_description = 'Ativa?'

    def title(self):
        if self.promo_type == PromotionType.PERCENT_OFF.name:
            return f"{int(self.value1)}% de desconto"
        return f"de R$ {localize(self.value1)} por R$ {localize(self.value2)}"
    title.short_description = 'Titulo'

    class Meta:
        verbose_name = 'Promoção'
        verbose_name_plural = 'Promoções'


class PromotionImage(models.Model):
    promotion = models.ForeignKey(
        Promotion, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField('imagem', upload_to='images')
    position = models.PositiveSmallIntegerField('posição', default=0)

    class Meta:
        verbose_name = 'Imagem promoção'
        verbose_name_plural = 'Imagens da promoção'

    def __str__(self):
        return str(self.position)
