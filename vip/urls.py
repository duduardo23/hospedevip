from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('s/<slug:slug>', views.store, name='store'),
    path('promocao/<int:promotion_id>', views.promotion, name='promotion'),
]