Create database: 
CREATE USER hospedevip WITH PASSWORD 'hospedevip';
create database hospedevip;
grant all privileges on database hospedevip to hospedevip;


pip install psycopg2
python -m pip install Pillow

# Create virtual env
  sudo pip install virtualenv
  virtualenv env1
  env1\Scripts\activate
  pip3 install -r requirements.txt

# Active  virtual env
  source env/bin/activate

# Update dependencies file:
  pip3 freeze > requirements.txt

# Rodar servidor desenvolvimento
  python manage.py runserver

# Create model change migrations
  python manage.py makemigrations vipapp
# Apply pending migration 
  python manage.py migrate
# Show migration sql code
  python manage.py sqlmigrate vip 0001

# Open Shell
  python manage.py shell

# Create admin user
  python manage.py createsuperuser


TODO:
[] Promoção:
  [] Desconto percetual valor total
  [] Desconto de valor
  [] Varias imagens
[] Categoria no estabelecimento
[] Pagina com detalhe do estabelecimento
[] Detalhe da promoção
[] Ver valor front end
[] Pensar em valor
[] Controle acesso




